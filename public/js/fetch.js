function fetchData() {
    fetch("http://localhost:3001/recipes").then(response => {
      console.log(response)
      response.json().then(
      data =>{
        console.log(data);
        if(data.length > 0 ) {
        var disp = "";
        data.forEach((urec) => {
            disp += '<div class="col-md-4 col-sm-4 col-xs-12 resp_mb50" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">';
            disp += '<div class="recipe_img"><img src="'+urec.images.full+'" alt="" class="w-100"></div></div>';
            disp += '<div class="col-md-8 col-sm-8 col-xs-12" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">';
            disp += '<h3 class="desh_ttl ds_font">'+urec.title+'</h3>';
            disp += '<p class="dish_desc mb-0">'+urec.description+'</p>';
            disp += '<div class="infodish">';
            disp += '<p class="mb-0"><i class="fa fa-cutlery" aria-hidden="true"></i>&nbsp;&nbsp;Servings: '+urec.servings+'</p>';
            disp += '<p class="mb-0"><i class="fa fa-spinner" aria-hidden="true"></i>&nbsp;&nbsp;Preparing time: '+urec.prepTime+'</p>';
            disp += '<p class="mb-0"><i class="fa fa-glass" aria-hidden="true"></i>&nbsp;&nbsp;Cooking time: '+urec.cookTime+'</p>';
            disp += '<p class="mb-0"><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;Post date: '+urec.postDate+'</p>';
            disp += '<p class="mb-0"><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit date: '+urec.editDate+'</p>';
            disp += '</div></div>';
            disp += '<div class="col-md-4 col-sm-4 col-xs-12"></div>';
            disp += '<div class="col-md-8 col-sm-8 col-xs-12 mb-5 resp_mb50 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000"">';
            disp += '<div class="ingr_box">';
            disp += '<h3 class="ds_font">Ingredients</h3>';
            for(let i=0; i<urec.ingredients.length; i++) {
                
                disp += '<div class="listitemingr">'+urec.ingredients[i].amount+'&nbsp;'+urec.ingredients[i].measurement+'&nbsp;of&nbsp;'+urec.ingredients[i].name+'</li></div>';
                
            }
            disp += '</div>';
            disp += '</div>';
        })
        document.getElementById("dishesd").innerHTML = disp;   
        }
      })
    }).catch(err => console.log(err));
}
fetchData();


